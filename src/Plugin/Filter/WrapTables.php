<?php

namespace Drupal\ma_filters\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\ma_filters\WrapFilterInterface;
use Drupal\ma_filters\WrapFilterTrait;

/**
 * MA Wrap Tables Filter class. Implements process() method only.
 *
 * @Filter(
 *   id = "filter_ma_wrap_tables",
 *   title = @Translation("MA Wrap Tables Filter"),
 *   description = @Translation("Wrap tables in WYSIWYG fields."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 * )
 */
class WrapTables extends FilterBase implements WrapFilterInterface {

  use WrapFilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    return 'table';
  }

  /**
   * {@inheritdoc}
   */
  public function getWrapper() {
    return 'div';
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses() {
    return 'table-wrap';
  }

}
