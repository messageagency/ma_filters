<?php

namespace Drupal\ma_filters\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\ma_filters\WrapFilterInterface;
use Drupal\ma_filters\WrapFilterTrait;


/**
 * MA Wrap Tables Filter class. Implements process() method only.
 *
 * @Filter(
 *   id = "filter_ma_link_span",
 *   title = @Translation("MA Link Span Filter"),
 *   description = @Translation("Wrap links with span element."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 * )
 */
class LinkSpan extends FilterBase implements WrapFilterInterface {
  use WrapFilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    return 'a';
  }

  /**
   * {@inheritdoc}
   */
  public function getWrapper() {
    return 'span';
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses() {
    return 'text';
  }

}
