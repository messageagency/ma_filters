<?php

namespace Drupal\ma_filters;

use Drupal\filter\FilterProcessResult;

/**
 * MA Filter Base class.
 */
trait WrapFilterTrait {

  /**
   * Setup libxml stuff.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    libxml_use_internal_errors(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (empty($text)) {
      return new FilterProcessResult($text);
    }

    $dom = $this->getDomDocument($text);
    try {
      $dom = $this->wrapDom($dom, $this->getTarget(), $this->getWrapper(), $this->getClasses());
    }
    catch (\Exception $e) {
      return new FilterProcessResult($text);
    }
    return new FilterProcessResult($this->getHtml($dom));
  }

  /**
   * Check which version of Libxml is in use.
   *
   * Older versions of libxml always add DOCTYPE, <html>, and <body> tags.
   * Sometimes, PHP is >= 5.4, but libxml is old enough that the constants are
   * not defined.
   *
   * @return bool
   *   Return true id using new LibXML.
   *
   * @see http://www.php.net/manual/en/libxml.constants.php
   */
  protected static function isNewLibXml() {
    return version_compare(PHP_VERSION, '5.4', '>=')
      && defined('LIBXML_HTML_NOIMPLIED') && defined('LIBXML_HTML_NODEFDTD');
  }

  /**
   * Generate a \DOMDocument from text.
   *
   * @param string $text
   *   Text to ingest.
   *
   * @return \DOMDocument
   *   DOMDocument object.
   */
  protected function getDomDocument($text) {
    // LibXML requires that the html is wrapped in a root node.
    $text = '<root>' . $text . '</root>';
    $dom = new \DOMDocument();

    if (self::isNewLibXml()) {
      $dom->loadHTML(htmlentities($text), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    }
    else {
      $dom->loadHTML(htmlentities($text));
    }
    return $dom;
  }

  /**
   * Given a DOMDocument $dom, wrap all $target elements with $wrapper.
   *
   * @return \DOMDocument
   *   DOMDcument.
   */
  protected function wrapDom(\DOMDocument $dom, $target, $wrapper, $classes = NULL) {
    // Find all tables in text or break.
    $elements = $dom->getElementsByTagName($target);
    if (empty($elements) || empty($elements->length)) {
      throw new \Exception(t('No @target elements found in source.', ['@target' => $target]));
    }

    // Wrap all the tables.
    foreach ($elements as $element) {
      $wrapper_element = $dom->createElement($wrapper);
      if (!empty($classes) && is_string($classes)) {
        $wrapper_element->setAttribute('class', $classes);
      }
      $element->parentNode->replaceChild($wrapper_element, $element);
      $wrapper_element->appendChild($element);
    }

    return $dom;
  }

  /**
   * Given DOMDoc with <root> element and all children, return rendered HTML.
   *
   * @param \DOMDocument $dom
   *   DOMDocument to convert.
   *
   * @return string
   *   Rendered HTML.
   */
  protected function getHtml(\DOMDocument $dom) {
    // Get innerHTML of root node.
    $html = "";
    foreach ($dom->getElementsByTagName('root')->item(0)->childNodes as $child) {
      // Re-serialize the HTML.
      $html .= $dom->saveHTML($child);
    }

    // For lower older libxml, use preg_replace to clean up DOCTYPE.
    if (!self::isNewLibXML()) {
      $html_start = strpos($html, '<html><body>') + 12;
      $html_length = strpos($html, '</body></html>') - $html_start;
      $html = substr($html, $html_start, $html_length);
    }
    return $html;
  }

}
