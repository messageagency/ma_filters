<?php

namespace Drupal\ma_filters;

/**
 * A filter for wrapping one HTML element with another. To use this tool,
 * implement this interface (see LinkSpan and WrapTables) and extend
 * WrapFilterBase
 */
interface WrapFilterInterface {

  /**
   * Return the target HTML element name to be wrapped.
   */
  public function getTarget();

  /**
   * Return the HTML element name with which to wrap the target.
   */
  public function getWrapper();

  /**
   * Return the value to be set for wrapper's class attribute
   */
  public function getClasses();

}