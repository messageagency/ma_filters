# README

## Filter Plugins

### WrapTables
- Enable the "Make tables responsive" text format filter to display field tables for better responsive containment.

## Creating new Filter Plugins
- Copy the example in src/Plugin/Filter/WrapTables.php